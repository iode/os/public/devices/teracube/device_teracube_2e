#
# Copyright (C) 2021 Teracube Inc.
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit device configuration.
$(call inherit-product, device/teracube/2e/device.mk)

# Inherit some common Lineage stuff
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

PRODUCT_BRAND := teracube
PRODUCT_MANUFACTURER := Teracube
PRODUCT_DEVICE := 2e
PRODUCT_NAME := lineage_2e
PRODUCT_MODEL := Teracube 2e

PRODUCT_GMS_CLIENTID_BASE := android-teracube
TARGET_VENDOR := teracube
TARGET_VENDOR_PRODUCT_NAME := 2e

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.build.stock_fingerprint="Teracube/Teracube_2e/Teracube_2e:10/QP1A.190711.020/202011161116:user/release-keys"
